package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.TaskClearRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить все задачи.";
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Очистка списка задач]");
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
    }

}
