package ru.tsc.kirillov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.NameComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Сортировка по имени", NameComparator.INSTANCE),
    BY_STATUS("Сортировка по статусу", StatusComparator.INSTANCE),
    BY_CREATED("Сортировка по дате создания", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Сортировка по дате начала", DateBeginComparator.INSTANCE);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return null;
        for (Sort sort: values()) {
            if (sort.name().equals(value))
                return sort;
        }

        return null;
    }

}
