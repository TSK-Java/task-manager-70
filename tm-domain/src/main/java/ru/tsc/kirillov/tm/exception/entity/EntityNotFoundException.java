package ru.tsc.kirillov.tm.exception.entity;

public final class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Ошибка! Объект не найден.");
    }

}
