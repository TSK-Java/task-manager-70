package ru.tsc.kirillov.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable
    Date getCreatedDate();

    void setCreatedDate(@Nullable Date created);

}
