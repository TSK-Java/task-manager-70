package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDto create(@Nullable String userId);

    @NotNull
    TaskDto create(@Nullable String userId, @NotNull String name);

    @NotNull
    TaskDto save(@Nullable String userId, @NotNull TaskDto task);

    @Nullable
    Collection<TaskDto> findAll(@Nullable String userId);

    @Nullable
    TaskDto findById(@Nullable String userId, @NotNull String id);

    void removeById(@Nullable String userId, @NotNull String id);

    void remove(@Nullable String userId, @NotNull TaskDto task);

    void remove(@Nullable String userId, @NotNull List<TaskDto> tasks);

    boolean existsById(@Nullable String userId, @NotNull String id);

    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
