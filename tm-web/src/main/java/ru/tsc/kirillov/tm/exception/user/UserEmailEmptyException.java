package ru.tsc.kirillov.tm.exception.user;

public final class UserEmailEmptyException extends AbstractUserException {

    public UserEmailEmptyException() {
        super("Ошибка! Email адрес пользователя не задан.");
    }

}
