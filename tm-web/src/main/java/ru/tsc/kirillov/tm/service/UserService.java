package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.kirillov.tm.dto.model.RoleDto;
import ru.tsc.kirillov.tm.dto.model.UserDto;
import ru.tsc.kirillov.tm.enumerated.RoleType;
import ru.tsc.kirillov.tm.exception.user.*;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private IUserDtoRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", "admin@domain.com", RoleType.ADMIN);
        initUser("test", "test", "test@domain.com", RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (email == null || email.isEmpty()) throw new UserEmailEmptyException();
        if (roleType == null) throw new UserRoleEmptyException();

        @Nullable final UserDto user = userRepository.findFirstByLogin(login);
        if (user != null) return;
        createUser(login, password, email, roleType);
    }

    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordEmptyException();
        if (roleType == null) throw new UserRoleEmptyException();

        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final UserDto user = new UserDto(login, passwordHash, email);
        @NotNull final RoleDto role = new RoleDto(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @NotNull
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new UserLoginEmptyException();

        @Nullable final UserDto user = userRepository.findFirstByLogin(login);
        if (user == null) throw new UserNotFoundException(login);
        return user;
    }

}